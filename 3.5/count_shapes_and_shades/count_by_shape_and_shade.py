import numpy as np
import cv2
import matplotlib.pyplot as plt
from skimage.measure import label, regionprops

def find_and_count_shapes(binary_image):
    labeled = label(binary_image)
    props = regionprops(labeled)
    
    total_shapes = len(props)
    shapes = {'rectangles': 0, 'circles': 0}
    colors = {'colors_rects': [], 'colors_circles': []}

    for prop in props:
        bbox = prop.bbox
        region_h = h[bbox[0]:bbox[2], bbox[1]:bbox[3]]

        if len(np.unique(region_h[0])) != 2:
            shapes['rectangles'] += 1
            colors['colors_rects'].append(np.unique(region_h)[0])
        else:
            shapes['circles'] += 1
            colors['colors_circles'].append(np.unique(region_h)[1])

    return total_shapes, shapes, colors


image_path = "balls_and_rects.png"

image = cv2.imread(image_path, cv2.IMREAD_COLOR)
gray_image = image.mean(2)
hsv = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
h = hsv[:, :, 0]
binary_image = (gray_image > 0).astype(np.uint8)

total_shapes, count_by_shapes, colors = find_and_count_shapes(binary_image)
print(f"Total number of objects in image: {total_shapes}")
print(f"shapes - {count_by_shapes}")
print(f"colors for Rectanlges: {len(np.unique(colors['colors_rects']))}")
print(f"colors for circles: {len(np.unique(colors['colors_circles']))}")
