import numpy as np

def load_image(filename: str):
    data = np.loadtxt(filename, skiprows=1, dtype='float')
    return data

def find_offset(image1, image2):
    obj_1 = np.argwhere(image1 == 1)
    obj_2 = np.argwhere(image2 == 1)
    
    if len(obj_1) == 0 or len(obj_2) == 0:
        return 'Объектов на изображении нет'
    
    offset = obj_2[0] - obj_1[0]
    return offset[1], offset[0]

img1 = load_image('img1.txt')
img2 = load_image('img2.txt')

print(find_offset(img1, img2))
