import numpy as np

def calculate_nominal_res(data, max_mm):
    obj_res = np.sum(np.max(data, axis=0))
    if obj_res:
        return f'Номинальное разрешение: {max_mm / obj_res}'
    else:
        return 'Фигура отсутствует в файле'

def load_figure(filename: str):
    with open(filename, 'r') as f:
        mm_max = float(next(f))
    data = np.loadtxt(filename, skiprows=1, dtype='float')
    return mm_max, data


for i in range(1, 6+1):
    try:
        print(f'загружаем figure{i}...')
        r = load_figure(f'figure{i}.txt')
        mm_max, data = r
        print(calculate_nominal_res(data, mm_max))
    except FileNotFoundError:
        print(f'figure{i}.txt не найден')


