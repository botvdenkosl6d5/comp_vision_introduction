import matplotlib.pyplot as plt
import numpy as np
from skimage.morphology import label
from skimage.measure import regionprops

def vline_recognition(arr, first_col = False):
    if first_col:
        return 1. in arr.mean(0)[:2]
    return 1. in arr.mean(0)


def recognize(prop):
    euler_num = prop.euler_number
    if euler_num == -1:  # две дырки, B или 8
        if vline_recognition(prop.image, first_col = True):
            return "B"
        else:
            return "8"
    elif euler_num == 0:  # одна - A, 0, P или D
        y, x = prop.centroid_local
        y /= prop.image.shape[0]
        x /= prop.image.shape[1]
        if euler_num == 0 and vline_recognition(prop.image, first_col = True):
            if prop.orientation == 0:
                return "D"
            return "P"

        if np.isclose(x, y, 0.06):
            if euler_num == 0 and not 1 in prop.image.mean(1):
                return "0"
            return "*"
        else:
            return "A"
    else:
        if prop.image.mean() == 1:
            return "-"
        else:
            mean_arr = prop.image.mean(0)
            
            if mean_arr[mean_arr == 1].shape[0] > 1:
                return '1'
            else:
                tmp = prop.image.copy()
                tmp[[0, -1], :] = 1
                tmp[:, [0, -1]] = 1
                tmp_labeled = label(tmp)
                tmp_props = regionprops(tmp_labeled)
                tmp_euler = tmp_props[0].euler_number
                if tmp_euler == -3:
                    return "X"
                elif tmp_euler == -1:
                    return "/"
                else:
                    if prop.eccentricity > 0.5:
                        return "W"
    return "*"

image = plt.imread('symbols.png').mean(2)
image = image > 0
labeled = label(image)
plt.imshow(labeled)
plt.show()

total_symbols = np.max(labeled)
print(f"Общее количество символов в изображении: {total_symbols}")

props = regionprops(labeled)
res = {}
for prop in props:
    symbol = recognize(prop)
    if symbol not in res:
        res[symbol] = 0
    res[symbol] += 1
print(res)

print("Процентное соотношение каждого символа:")
for symbol, count in res.items():
    percentage = (count / total_symbols) * 100
    print(f"{symbol}: {percentage}%")
print(f'Общий процент распознавания: {(1 - res.get("?", 0) / total_symbols) * 100}%')