import numpy as np
from scipy.ndimage import binary_hit_or_miss

def count_objects_with_masks(image, masks):
    total_objects = 0
    object_counts = {}
    for i, mask in enumerate(masks):
        obj_count = np.sum(binary_hit_or_miss(image, mask))
        total_objects += obj_count
        object_counts[f'object {i + 1}'] = obj_count
    return total_objects, object_counts


obj1_mask = np.array([[1, 1, 1, 1],
                  [1, 1, 1, 1],
                  [1, 1, 0, 0],
                  [1, 1, 0, 0],
                  [1, 1, 1, 1],
                  [1, 1, 1, 1]])

obj2_mask = np.array([[1, 1, 1, 1],
                  [1, 1, 1, 1],
                  [0, 0, 1, 1],
                  [0, 0, 1, 1],
                  [1, 1, 1, 1],
                  [1, 1, 1, 1]])

obj3_mask = np.array([[1, 1, 0, 0, 1, 1],
                  [1, 1, 0, 0, 1, 1],
                  [1, 1, 1, 1, 1, 1],
                  [1, 1, 1, 1, 1, 1]])

obj4_mask = np.array([[1, 1, 1, 1, 1, 1],
                  [1, 1, 1, 1, 1, 1],
                  [1, 1, 0, 0, 1, 1],
                  [1, 1, 0, 0, 1, 1]])

obj5_mask = np.ones((4, 6))

masks = [obj1_mask, obj2_mask, obj3_mask, obj4_mask, obj5_mask]
image = np.load("psnpy.txt")
total_objects, mask_counts = count_objects_with_masks(image, masks)
for i in mask_counts:
    print(f'Amount of {i}:', mask_counts[i])
print('Total objects:', total_objects)