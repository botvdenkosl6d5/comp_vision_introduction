import matplotlib.pyplot as plt
from skimage.measure import label, regionprops
from skimage.morphology import remove_small_objects
from skimage.filters import threshold_otsu


def find_and_count_pencils(images):
    image = plt.imread(images)
    gray_image = image.mean(2)

    thresh = threshold_otsu(gray_image)
    binary_image = gray_image < thresh
    
    binary_image = remove_small_objects(binary_image, min_size = 5000)
    labeled_image = label(binary_image)
    props = regionprops(labeled_image)

    pencils_count = 0
    for prop in props:
        if 2300 < prop.major_axis_length and prop.area > 200000:
            pencils_count += 1

    plt.imshow(gray_image, cmap='gray')
    plt.title(f'Pencils in this image: {pencils_count}')
    plt.show()

    return pencils_count


image_folder = "images"
image_count = 12
total_pencils_count = 0

images = [f"{image_folder}/img ({i}).jpg" for i in range(1, image_count + 1)]
for image in images:
    pencils_count = find_and_count_pencils(image)
    total_pencils_count += pencils_count

print(f'Total pencils count in images: {total_pencils_count}')
