import numpy as np
import matplotlib.pyplot as plt
from skimage.morphology import label
from skimage.measure import label, regionprops

frames_folder = "frames"
num_frames = 100

frames_data = [np.load(f"{frames_folder}/h_{frame}.npy") for frame in range(num_frames)]

def get_trajectory(image):
    binary_image = image > 0
    labeled_image = label(binary_image)
    props = sorted(regionprops(labeled_image), key=lambda regionprop: regionprop.area)
    centroids = [prop.centroid for prop in props]
    return np.array(centroids)

ax = plt.subplot()

x_coords, y_coords = [[] for _ in range(len(frames_data[0]))], [[] for _ in range(len(frames_data[0]))]

for frame_data in frames_data:
    trajectory = get_trajectory(frame_data)
    for obj_index in range(trajectory.shape[0]):
        x_coords[obj_index].append(trajectory[obj_index, 0])
        y_coords[obj_index].append(trajectory[obj_index, 1])

for obj_index in range(len(frames_data[0])):
    ax.plot(x_coords[obj_index], y_coords[obj_index])

ax.set_xlabel('X Coordinate')
ax.set_ylabel('Y Coordinate')
ax.set_title('Object Trajectories')
plt.show()
